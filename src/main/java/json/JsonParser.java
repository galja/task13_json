package json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import candiespack.Candy;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@JsonIgnoreProperties("wrapper")
public class JsonParser {
    private ObjectMapper objectMapper;

    public JsonParser() {
        this.objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }
    File json = new File("src/main/resources/candies.json");
    File schema = new File("src/main/resources/candiesSchema.json");


    static class Cand {
        private List<Candy> candies;

        public List<Candy> getCandies() {
            return candies;
        }

        public void setCandies(List<Candy> candies) {
            this.candies = candies;
        }

    }

    public void get() throws IOException {
        Cand c = objectMapper.readValue(json, Cand.class);
        for (Candy candy: c.getCandies()) {
            System.out.println(candy);
        }
    }

    public List<Candy> getCandiesList(File jsonFile){
        Candy[] candies = new Candy[0];
        try{
            candies = objectMapper.readValue(jsonFile, Candy[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(candies);
    }
}

