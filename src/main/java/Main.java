import candiespack.CandyComparator;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import candiespack.Candy;
import json.JsonParser;
import json.Validator;

public class Main {

    public static void main(String[] args) {
        File json = new File("src/main/resources/candies.json");
        File schema = new File("src/main/resources/candiesSchema.json");

        try {
            Validator.validate(json,schema);
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }

        JsonParser parser = new JsonParser();
//        printList(parser.getCandiesList(json));
        try {
            parser.get();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printList(List<Candy> candies) {
        Collections.sort(candies, new CandyComparator());
        System.out.println("JSON");
        for (Candy candy : candies) {
            System.out.println(candy);
        }
    }
}
